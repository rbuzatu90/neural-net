#pip install theano numpy pillow

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from keras.optimizers import SGD
from keras.models import load_model

def generate_weights():
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    
    # Definim structura datelor
    X_train = X_train.reshape(X_train.shape[0], 1, 28, 28)
    X_test = X_test.reshape(X_test.shape[0], 1, 28, 28)
    
    Y_train = np_utils.to_categorical(y_train, 10)
    Y_test = np_utils.to_categorical(y_test, 10)

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    
    #Definim arhitectura
    model = Sequential()
     
    model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1,28,28)))
    model.add(Convolution2D(32, 3, 3, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    print "Data before dividing: "
    print X_train[1]
    X_train /= 255
    print "Data after dividing: "
    print X_train[1]

    X_test /= 255

    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))
    
    sgd = SGD(lr=0.01, clipvalue=0.5)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    
    
    model.fit(X_train, Y_train, batch_size=32, nb_epoch=2, verbose=1)
    print "Writing model.keras in current dir"
    model.save('model.keras')


def load_custom_model():
    print "Loading model.keras from current dir"
    model = load_model('model.keras')
    print "Loaded"
    print model
    #model.predict(x)


def test_on_new_img():
    print "Predict on ne data"
    from PIL import Image
    # Preprocesam imaginea, convertim in grayscale si micsoram imaginea la 28x28 pixeli
    img = Image.open('rsz_pic7.png')
    img = img.convert('L')
    img = img.resize((28,28))
    # img.show()
    new_data = np.asarray(img, dtype='float32')
    new_data = (new_data - 255) * (-1)
    # new_data = new_data.transpose(0, 1)
    new_data = np.expand_dims(new_data, axis=0)
    new_data = new_data.reshape(1,1,28,28)

    model = load_model('model.keras')
    result = model.predict(new_data)
    # print result.size
    # print result.shape
    print result
    # print result[0]


if __name__ == '__main__':
    # Se apeleaza generate_weights() o singura data pentru a calcula weights si pt a salva modelul, ulteroior se poate apela doar test_on_new_img
    generate_weights()
    #load_custom_model()
    test_on_new_img()
